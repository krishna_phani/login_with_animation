import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'login screen';
  userType:string='';
  usernameEntered:string='';
  passwordEntered:string='';
  usernameSet:boolean=true;
  passwordSet:boolean=true;
  userTypeSelected:boolean=true;
  agreedToTerms:boolean=false;
  validUser:string="user";
  validPassword:string="111";
  showCustomerCheckBox: boolean=false;
  showClientCheckBox: boolean=false;  
  validDetails:boolean=false;
  invalidDetails:boolean=true;
  validDetailsDisplay:boolean=false;
  invalidDetailsDisplay:boolean=false;
  supressCheckBox:boolean=true;
  boxChecked=false;
  action(){
    this.userTypeSelected=true;
    this.boxChecked=false;
    if(this.userType=="customer")
    {
      this.showCustomerCheckBox=true;
      this.showClientCheckBox=false;
      this.agreedToTerms=false;
    }
    else{
      this.showClientCheckBox=true;
      this.showCustomerCheckBox=false;
      this.agreedToTerms=false;
    }
  }
  clientCheck() {
    this.agreedToTerms=false;
    this.invalidDetailsDisplay=false;
    this.showCustomerCheckBox=false;
    if (this.boxChecked==true)
      this.agreedToTerms=true;
  }

  customerCheck() {
    this.agreedToTerms=false;
    this.invalidDetailsDisplay=false;
    this.showClientCheckBox=false;
    if (this.boxChecked==true)
      this.agreedToTerms=true;
  }

  rend(){
    this.invalidDetailsDisplay=false;
    this.usernameSet=true;
    this.passwordSet=true;
    this.userTypeSelected=true;
    this.supressCheckBox=true;

    if(this.usernameEntered.length==0){
      this.usernameSet=false;
    }
    else {
      if(this.passwordEntered.length==0){
        this.passwordSet=false;
      }
      else {
        if(this.userType.length==0){
            this.userTypeSelected=false;
        } else {
          if (this.agreedToTerms==true) {
            this.validateCredentials();
          } else {
            this.supressCheckBox=false;
          }
        }
      }
    }
  }

  validateCredentials() {
    if ( (this.usernameEntered===this.validUser)  && (this.passwordEntered===this.validPassword) && (this.agreedToTerms=true) ){
      this.validDetails=true;
      this.invalidDetails=false
    } else {
      this.invalidDetailsDisplay = true;
    }
  }

  back(){
    this.invalidDetails=true;
    this.validDetails=false;
  }

  new(a : any){
    this.usernameEntered=this.namesss[a];
    this.invalidDetails=true;
    this.validDetails=false;

  }


 public namesss=["akshay","krishna","mukesh"];
 public msg=["heyyy", "hellow", "byeee"];



}